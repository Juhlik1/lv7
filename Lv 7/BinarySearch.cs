﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lv_7
{
    class BinarySearch : ISearchStrategy
    {
        public bool Search(double[] array, double x)
        {
            int lowerBound = 0;
            int upperBound = array.Length;
            while (lowerBound <= upperBound)
            {
                int midPoint = (lowerBound + upperBound) / 2;
                if (array[midPoint] > x)
                    upperBound = midPoint - 1;
                else if (array[midPoint] < x)
                    lowerBound = midPoint + 1;
                else return true;
            }
            return false;
        }
    }
}
