﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lv_7
{
    interface ISearchStrategy
    {
        bool Search(double[] array, double x);
    }
}
