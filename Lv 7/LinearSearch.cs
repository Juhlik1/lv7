﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lv_7
{
    class LinearSearch : ISearchStrategy
    {
        public bool Search(double[] array, double x)
        {
            foreach (Double value in array)
            {
                if (value == x)
                {
                    return true;
                }
            }
            return false;
        }
    }
}

